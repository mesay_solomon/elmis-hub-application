package org.jsi.elmis.hub.request.service;

import java.sql.*;

class RequestNotifier extends Thread {

	private Connection conn;

	public RequestNotifier(Connection conn) {
		this.conn = conn;
	}

	public void run() {
		while (true) {
			try {
				Statement stmt = conn.createStatement();
				stmt.execute("SELECT * FROM elmis_ws_requests");
				stmt.close();
				Thread.sleep(2000);
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}

}