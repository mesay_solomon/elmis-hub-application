package org.jsi.elmis.hub.request.service;

import org.jsi.elmis.hub.utils.Properties;

public class ElmisConstants {
	public static final String CENTRAL_ELMIS_URL = Properties.getInstance().getProperty("ceLMISBaseURL");
	
	//rnr
	public static final String REQUEST_TYPE_RNR = "RNR";
	public static final String REQUEST_STATUS_PENDING = "PENDING";
	public static final String REQUEST_STATUS_SUCCESS = "SUCCESS";
	public static final String REQUEST_STATUS_FAIL = "FAIL";
	public static final String REQUEST_NAME_SUBMIT_RNR = "RNR";
}
