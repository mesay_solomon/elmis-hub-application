package org.jsi.elmis.hub.request.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEntity {
    private int status;//TODO: elaborate response object. For the time being, let's suppose 0 status is success
    private String response;
}