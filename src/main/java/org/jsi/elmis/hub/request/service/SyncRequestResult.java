package org.jsi.elmis.hub.request.service;

public class SyncRequestResult {
	private Boolean success;
	private String remark;
	public SyncRequestResult(Boolean sccs,String rmrk){
		this.success = sccs;
		this.remark = rmrk;
	}
	
	public Boolean getSuccess() {
		return success;
	}
	
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
	public String getRemark() {
		return remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
