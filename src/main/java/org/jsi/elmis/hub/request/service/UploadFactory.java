package org.jsi.elmis.hub.request.service;

import org.jsi.elmis.hub.model.rnr.Report;

import com.google.gson.Gson;

public class UploadFactory extends BaseFactory {

	public static Long UploadRnR(Report report) throws Exception {
		Gson gson = new Gson();
		String json = gson.toJson(report, report.getClass());
		return (Long) uploadJSON("requisitions", "R&R", json, Long.class);
	}

	public static void main(String args[]) {
		System.out.println("Test");
	}
}