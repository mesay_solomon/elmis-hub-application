package org.jsi.elmis.hub.rest.request;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.jsi.elmis.hub.model.ProcessingPeriod;
/**
 * 
 * @author Mekbib
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeeklyPeriodsRequest {
	private Integer parentPeriodId;
	private ArrayList<ProcessingPeriod> weeklyPeriods;
}
