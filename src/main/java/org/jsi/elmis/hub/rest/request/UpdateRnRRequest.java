package org.jsi.elmis.hub.rest.request;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.jsi.elmis.hub.model.rnr.HubRnR;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateRnRRequest {
	private List<HubRnR> rnrList = new ArrayList<>();
	private String status;
}
