package org.jsi.elmis.hub.dao;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.jsi.elmis.hub.mappers.HubRnRMapper;
import org.jsi.elmis.hub.model.Facility;
import org.jsi.elmis.hub.model.ProcessingPeriod;
import org.jsi.elmis.hub.model.User;
import org.jsi.elmis.hub.model.rnr.HubRnR;

public class HubRnRDAO {
	private HubRnRDAO() {
		setUpDBSession();
	};

	// Instance returned
	private static HubRnRDAO hubRnRDAO = null;

	private SqlSessionFactory sqlMapper = null;
	private SqlSession session = null;
	Reader reader = null;

	public static HubRnRDAO getInstance() {
		if (hubRnRDAO == null) {
			hubRnRDAO = new HubRnRDAO();
		}
		return hubRnRDAO;
	}

	private void setUpDBSession() {
		String resource = "org/elmis/sdp/ws/configuration.xml";
		try {
			reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader);
			session = sqlMapper.openSession();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public SqlSession getSession() {
		return session;
	}

	// -------------------------------------------------

	public Integer insertRnR(HubRnR hubRnR) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		Integer result = 0;
		try {
			result = mapper.insertRnR(hubRnR);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
		return result;
	}
	
	public List<Facility> getFacilities(){
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		List<Facility> facilities = null;

		try {
			facilities = mapper.selectFacilities();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilities;
	}

	public void updateRnR(HubRnR hubRnR) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		try {
			mapper.updateRnR(hubRnR);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public Integer updateUser(User user) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		Integer result = null;

		try {
			result = mapper.updateUser(user);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
			return result;
		}
	}
	
	public Integer insertUser(User user) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		Integer result = null;

		try {
			result = mapper.insertUser(user);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
			return result;
		}
	}

	public ArrayList<ProcessingPeriod> fetchPeriodsToSubmitRnRFor() {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ArrayList<ProcessingPeriod> periods = null;
		try {

			periods = mapper.selectPeriodsToSubmitRnRFor();

			for (ProcessingPeriod processingPeriod : periods) {
				System.out.println(processingPeriod.getStartdate()+ ":" + processingPeriod.getEnddate());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periods;
	}
	
	public ProcessingPeriod getProcessingPeriod(Integer id) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ProcessingPeriod period = null;
		try {

			period = mapper.selectPerocessingPeriod(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}
	
	public User findUser(String userName) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		User user = null;
		try {

			user = mapper.selectUser(userName);

			if(user !=  null)
			System.out.println(user.getUserName());

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return user;
	}
	
	public List<User> findAllUsers() {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		List<User> users = null;
		try {

			users = mapper.selectAllUsers();

			for (User user2 : users) {
				System.out.println(user2.getUserName());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return users;
	}

	public List<HubRnR> selectMontlyRnRsForPeriod(Date startDate, Date endDate) {

		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		List<HubRnR> rnrs = null;
		try {

			rnrs = mapper.selectMontlyRnRsForPeriod(startDate, endDate);

			for (HubRnR hubRnR : rnrs) {
				System.out.println(hubRnR.getFacilityCode());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return rnrs;
	}
	
	public Integer findScheduleIdByPeriodId(Integer periodId) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		Integer scheduleId = null;
		try {

			scheduleId = mapper.findScheduleIdByPeriodId(periodId);

			if(scheduleId !=  null)
			System.out.println(scheduleId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return scheduleId;
	}
	
	public Boolean insertWeeklyPeriod(ProcessingPeriod weeklyPeriod) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		Boolean status = false;
		try {
			mapper.insertWeeklyProcessingPeriod(weeklyPeriod);
			status = weeklyPeriod.getId() != 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			status = false;
		} finally {
			session.commit();
			session.close();
		}
		return status;
	}
	
	public ArrayList<ProcessingPeriod> fetchWeeklyPeriods() {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ArrayList<ProcessingPeriod> periods = null;
		try {

			periods = mapper.selectWeeklyPeriods();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periods;
	}
	public List<HubRnR> selectAllRnRs(String programCode , String facilityCode, String type) {

		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		List<HubRnR> rnrs = null;
		try {

			rnrs = mapper.selectAllRnRs(programCode , facilityCode , type);

			for (HubRnR hubRnR : rnrs) {
				System.out.println(hubRnR.getFacilityCode());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return rnrs;
	}

	public static void main(String args[]) {
		// new HubRnRDAO().selectRnRsForPeriod(null, null);
	}

	public ProcessingPeriod getProcessingPeriodByRange(Date startDate,
			Date endDate) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ProcessingPeriod period = null;
		try {

			period = mapper.selectPerocessingPeriodByRange(startDate , endDate);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}

	public List<ProcessingPeriod> fetchAllPeriods() {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ArrayList<ProcessingPeriod> periods = null;
		try {

			periods = mapper.selectAllPeriods();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periods;
	}

	public List<HubRnR> selectMontlyRnRsForPeriod(Integer periodId) {
			SqlSession session = sqlMapper.openSession();
			HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

			List<HubRnR> rnrs = null;
			try {

				rnrs = mapper.selectMontlyRnRsForPeriodByPeriodId(periodId);

				for (HubRnR hubRnR : rnrs) {
					System.out.println(hubRnR.getFacilityCode());
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				session.close();
			}
			return rnrs;
		}
	public int updateRnRStatus(List<HubRnR> hubRnRList, String status) {
		SqlSession session = sqlMapper.openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		try {
			for(HubRnR rnr : hubRnRList){
				rnr.setStatus(status);
				mapper.updateRnRStatus(rnr);	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		} finally {
			session.commit();
			session.close();
		}
		return 1;
	}
}
