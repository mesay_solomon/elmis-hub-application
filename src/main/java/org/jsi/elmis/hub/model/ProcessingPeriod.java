package org.jsi.elmis.hub.model;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProcessingPeriod {
	
	private Integer id;
	
	private String name;
	
	private Date startdate;
	
	private Date enddate;
	
	private Integer parentperiodid;
	
	private Integer scheduleid;
	
	private String scheduleCode;

}
