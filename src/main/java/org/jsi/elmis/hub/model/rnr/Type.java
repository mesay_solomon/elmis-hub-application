package org.jsi.elmis.hub.model.rnr;

public class Type {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
