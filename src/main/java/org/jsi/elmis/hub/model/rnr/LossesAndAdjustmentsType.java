package org.jsi.elmis.hub.model.rnr;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LossesAndAdjustmentsType {

    private Integer id;
    private String name;
    private String description;
    private Boolean additive;
    private Integer displayOrder;
}

