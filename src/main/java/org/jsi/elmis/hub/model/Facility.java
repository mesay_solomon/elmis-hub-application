package org.jsi.elmis.hub.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Facility {
	
	private Integer id;
	
	private String code;
	
	private String name;

}
