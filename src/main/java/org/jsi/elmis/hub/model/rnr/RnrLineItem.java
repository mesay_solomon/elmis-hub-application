package org.jsi.elmis.hub.model.rnr;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
public class RnrLineItem implements Cloneable {

    private String productCode;

    private Integer beginningBalance;
    private Integer quantityReceived;
    private Integer quantityDispensed;
    private List<LossesAndAdjustments> lossesAndAdjustments = new ArrayList<>();

    private Integer stockInHand;
    private Integer stockOutDays;
    private Integer newPatientCount;
    private Integer quantityRequested;
    private String reasonForRequestedQuantity;

    private Integer amc;
    private Integer normalizedConsumption;
    private Integer calculatedOrderQuantity;
    private Integer maxStockQuantity;

    private Integer quantityApproved;
    private String remarks;
    
    public Object clone(){  
        try{  
            return super.clone();  
        }catch(Exception e){ 
            return null; 
        }
    }
    
    public static RnrLineItem getBlankRnRLineItem(){
    	RnrLineItem item = new RnrLineItem();
    	item.beginningBalance = 0; item.quantityReceived = 0; item.quantityDispensed = 0; item.stockInHand = 0; item.stockOutDays = 0;
    	item.quantityRequested = 0; item.amc = 0; item.newPatientCount = 0 ; item.normalizedConsumption = 0; item.calculatedOrderQuantity = 0;
    	item.maxStockQuantity = 0; item.quantityApproved = 0; item.remarks = ""; item.reasonForRequestedQuantity = "";
    	return item;
    }
}

