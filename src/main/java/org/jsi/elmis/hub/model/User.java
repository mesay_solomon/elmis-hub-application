package org.jsi.elmis.hub.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class User {
	
	private Integer id;
	
	private String userName;
	
	private String firstName;
	
	private String lastName;
	
	private String password;
	
	private Boolean active;

}
