package org.jsi.elmis.hub.model.rnr;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.jsi.elmis.hub.model.Facility;
import org.jsi.elmis.hub.model.ProcessingPeriod;
import org.jsi.elmis.hub.services.RnRService;
import org.jsi.elmis.hub.services.RnRServiceImpl;

import com.google.gson.Gson;

@Data
@NoArgsConstructor
public class HubRnR {
	
	public static final String WEEKLY = "WEEKLY";
	public static final String FACILITY_MONTHLY = "FACILITY_MONTHLY";
	public static final String AGGREGATED = "AGGREGATED";
	
	public static final String PENDING = "PENDING";
	public static final String SUBMITTED = "SUBMMITTTED";
	
	private Integer id;
	
	private String facilityCode;
	
	private Facility facility;
	
	private ProcessingPeriod period;
	
	private String programCode;
	
	private String type;
	
	private String rnrJSON;
	
	private String status;
	
	private HubRnR parentRnR;
	
	private Date startDate;
	
	private Date endDate;
	
	private Report report;
	
	public HubRnR(String rnr,String type , String status){
		Gson gson = new Gson();
    	Report report = gson.fromJson(rnr, Report.class);
    	this.report = report;
    	this.facilityCode = report.getAgentCode();
    	this.programCode = report.getProgramCode();
    	this.type = type;
    	this.rnrJSON = rnr;
    	RnRService rnrService = new RnRServiceImpl();
    	this.period = rnrService.getProcessingPeriod(report.getPeriodId());
    	if(period != null){
    		this.startDate = this.period.getStartdate();
    		this.endDate = this.period.getEnddate();
    	}
    	this.status = status;
	}
	
	public HubRnR(String rnr , String status){
		Gson gson = new Gson();
    	Report report = gson.fromJson(rnr, Report.class);
    	this.report = report;
    	this.facilityCode = report.getAgentCode();
    	this.programCode = report.getProgramCode();
    	this.rnrJSON = rnr;
    	RnRService rnrService = new RnRServiceImpl();
    	this.period = rnrService.getProcessingPeriod(report.getPeriodId());
    	this.type = this.period.getScheduleCode();
    	if(period != null){
    		this.startDate = this.period.getStartdate();
    		this.endDate = this.period.getEnddate();
    	}
    	this.status = status;
	}
	
	public HubRnR(Report report,String type , String status){
		this.report = report;
    	this.facilityCode = report.getAgentCode();
    	this.programCode = report.getProgramCode();
    	this.type = type;
    	Gson gson = new Gson();
    	this.rnrJSON = gson.toJson(report);
    	RnRService rnrService = new RnRServiceImpl();
    	this.period = rnrService.getProcessingPeriod(report.getPeriodId());
    	if(period != null){
	    	this.startDate = this.period.getStartdate();
	    	this.endDate = this.period.getEnddate();
    	}
    	this.status = status;
	}

}
