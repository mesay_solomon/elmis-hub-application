package org.jsi.elmis.hub.model.rnr;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LossesAndAdjustments  {

    public LossesAndAdjustmentsType type;
    private Integer quantity;
}