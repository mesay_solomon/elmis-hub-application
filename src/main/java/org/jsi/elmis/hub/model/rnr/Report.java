package org.jsi.elmis.hub.model.rnr;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import org.jsi.elmis.hub.model.ProcessingPeriod;

@Data
@NoArgsConstructor
public class Report {

  private String agentCode;
  private Integer periodId;
  private String programCode;
  private String approverName;
  private Boolean emergency;
  private List<RnrLineItem> products;


  public static void getBlankRnR(){
	  Report report = new Report();
	  
  }

    public void validate() throws Exception {

    }


}
