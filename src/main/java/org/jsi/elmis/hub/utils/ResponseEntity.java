package org.jsi.elmis.hub.utils;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 6/30/13
 * Time: 5:35 PM
 */
public class ResponseEntity {
    private int status;
    private String response;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}