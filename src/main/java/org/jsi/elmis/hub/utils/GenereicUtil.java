package org.jsi.elmis.hub.utils;

import java.io.FileInputStream;
import java.util.Properties;

public class GenereicUtil {
	public static Properties getProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(
					"Programmproperties.properties");
			prop.load(fis);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return prop;
	}

}
