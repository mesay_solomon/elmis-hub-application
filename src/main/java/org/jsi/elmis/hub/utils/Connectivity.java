package org.jsi.elmis.hub.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

public class Connectivity {
	public static void main(String args[]) {
		System.out.println(isReachable("http://uat.tz.elmis-dev.org/"));
//		System.out.println(isReachableSocket("http://uat.tz.elmis-dev.org/", 80));
//		System.out.println(isReachableCheckResponse("http://uat.tz.elmis-dev.org/"));

	}

	public static boolean isReachable(String urlString) {
		try {
			final URL url = new URL(urlString);
			final URLConnection conn = url.openConnection();
			conn.connect();
			return true;
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			return false;
		}
	}

	@SuppressWarnings("unused")
	private static boolean isReachableSocket(String host, Integer port) {
		Socket socket = null;
		boolean reachable = false;
		try {
			try {
				socket = new Socket(host, port);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			reachable = true;
		} finally {
			if (socket != null)
				try {
					socket.close();
				} catch (IOException e) {
				}
		}
		return reachable;
	}

	public static boolean isReachableCheckResponse(String url) {
		boolean isReachable = false;
		HttpURLConnection connection = null;
		try {
			URL u = new URL(url);
			connection = (HttpURLConnection) u.openConnection();
			connection.setRequestMethod("HEAD");
			int code = connection.getResponseCode();
			if (code == 200) {
				isReachable = true;
			}
			System.out.println("" + code);
			// You can determine on HTTP return code received. 200 is success.
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return isReachable;
	}
}
