package org.jsi.elmis.hub.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created with IntelliJ IDEA.
 * User: mesay
 * Date: 5/22/14
 * Time: 10:21 AM
 * To change this template use File | Settings | File Templates.
 */

public class Properties {
    private static java.util.Properties props = null;
    private static Properties properties = null;

    private Properties(){
        loadProperties();
    }

    public static Properties getInstance(){
            if (properties == null){
                  properties = new Properties();
            }
        return properties;
    }

    private static java.util.Properties loadProperties() {
        props = new java.util.Properties();
        try {
/*         File jarPath=new File(WSClientApp.class.getProtectionDomain().getCodeSource().getLocation().getPath());
           String propertiesPath=jarPath.getParentFile().getAbsolutePath();
           System.out.print("\n\n"+propertiesPath+"\n\n");*/
            FileInputStream fis = new FileInputStream("application.properties");
            props.load(fis);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return props;
    }

    private static java.util.Properties reLoadProperties() {
        loadProperties();
        return props;
    }
    
    public java.util.Properties getProperties(){
        return props;
   }
    
    public String getProperty(String key){
         return props.getProperty(key);
    }
    
    public void setProperty(String key, String value){
    	props.setProperty(key, value);
    	try {
			props.store(new FileOutputStream("application.properties"), null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
