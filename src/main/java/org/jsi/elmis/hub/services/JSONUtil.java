package org.jsi.elmis.hub.services;

import java.util.ArrayList;

import org.jsi.elmis.hub.model.rnr.HubRnR;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JSONUtil {
	
	public static ArrayList<HubRnR> parseHubRnRs(String json){
		ArrayList<HubRnR> rnrs = null;
		Gson gson = new Gson();
    	try{
    		rnrs = gson.fromJson(json, new TypeToken<ArrayList<HubRnR>>(){}.getType());
    		
    	}catch(Exception ex){
    		
    	}
		
		return rnrs;
	}

}
