package org.jsi.elmis.hub.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.hub.dao.HubRnRDAO;
import org.jsi.elmis.hub.model.Facility;
import org.jsi.elmis.hub.model.ProcessingPeriod;
import org.jsi.elmis.hub.model.User;
import org.jsi.elmis.hub.model.rnr.HubRnR;
import org.jsi.elmis.hub.model.rnr.LossesAndAdjustments;
import org.jsi.elmis.hub.model.rnr.Report;
import org.jsi.elmis.hub.model.rnr.RnrLineItem;
import org.jsi.elmis.hub.request.service.BaseFactory;
import org.jsi.elmis.hub.request.service.LoginRequestResult;
import org.jsi.elmis.hub.rest.result.RnRSubmissionResult;
import org.jsi.elmis.hub.utils.Connectivity;
import org.springframework.beans.factory.annotation.Value;

import us.monoid.json.JSONException;

import com.google.gson.Gson;

public class RnRServiceImpl implements RnRService{
	
	@Value("${hub.facility.code}")
	private String hubFacilityCode;
	@Value("${hub.approver.code}")
	private String hubApproverCode;
	@Value("${elmis.central.url}")
	private String eLMISCentralURL;

	@Override
	public String aggregateRnR(ArrayList<String> rnrs , ArrayList<String> facilityCodes , Integer periodId){
		Gson gson = new Gson();
		ArrayList<Report> reports = new ArrayList<Report>();
		for (String rnr : rnrs) {
			Report report = gson.fromJson(rnr, Report.class);
			reports.add(report);
		}
		
		ArrayList<Report> monthlyFacilityRnRs = new ArrayList<Report>();
		
		for (String facilityCode : facilityCodes) {
			ArrayList<Report> facilityRnRs = getFacilityRnRs(reports, facilityCode);
			if(!facilityRnRs.isEmpty()){
				Report monthlyReport = aggregateWeeklyFacilityRnRs(facilityRnRs , facilityCode , periodId);
				HubRnR hubRnRMonthly = new HubRnR(monthlyReport, HubRnR.FACILITY_MONTHLY, HubRnR.PENDING);
				HubRnRDAO.getInstance().insertRnR(hubRnRMonthly);
				monthlyFacilityRnRs.add(monthlyReport);
				//
			}
		}

		Report aggregatedReport = aggregateMonthlyRnRsForFacilities(monthlyFacilityRnRs , periodId);
		HubRnR hubRnRAggregated = new HubRnR(aggregatedReport, HubRnR.AGGREGATED, HubRnR.PENDING);
		HubRnRDAO.getInstance().insertRnR(hubRnRAggregated);
		String aggregatedReportJSON = gson.toJson(aggregatedReport); 
		System.out.println(aggregatedReportJSON);
		System.out.println("aggregation complete");
		return aggregatedReportJSON;
	}
	
	@Override
	public String aggregateMonthlyRnR(ArrayList<String> rnrs , Integer periodId){
		Gson gson = new Gson();
		ArrayList<Report> reports = new ArrayList<Report>();
		for (String rnr : rnrs) {
			Report report = gson.fromJson(rnr, Report.class);
			reports.add(report);
		}
	
		Report aggregatedReport = aggregateMonthlyRnRsForFacilities(reports , periodId);
		HubRnR hubRnRAggregated = new HubRnR(aggregatedReport, HubRnR.AGGREGATED, HubRnR.PENDING);
		HubRnRDAO.getInstance().insertRnR(hubRnRAggregated);
		String aggregatedReportJSON = gson.toJson(aggregatedReport); 
		System.out.println(aggregatedReportJSON);
		System.out.println("aggregation complete");
		return aggregatedReportJSON;
	}
	
	public ArrayList<Report> getFacilityRnRs(ArrayList<Report> rnrs, String facilityCode){
		ArrayList<Report> facilityRnRs = new ArrayList<Report>();
		for (Report report : rnrs) {
			if(report.getAgentCode().equalsIgnoreCase(facilityCode)){
				facilityRnRs.add(report);
			}
		}
		return facilityRnRs;
	}
	
	public List<String> getFacilityCodes(){
		List<Facility> facilities = HubRnRDAO.getInstance().getFacilities();
		List<String> facilityCodes = new ArrayList<String>();
		for (Facility facility : facilities) {
			facilityCodes.add(facility.getCode());
		}
		return facilityCodes;
	}
	
	public Report aggregateWeeklyFacilityRnRs(ArrayList<Report> reports , String facilityCode , Integer periodId){
		Report aggregatedReport = new Report();
		aggregatedReport.setProducts(new ArrayList<RnrLineItem>());
		List<String> productCodes = getProductCodes(reports);
		
		for (String code : productCodes) {
			RnrLineItem aggregatedLineItem = RnrLineItem.getBlankRnRLineItem();
			aggregatedLineItem.setProductCode(code);
			
			Integer currentIdx = 0;
			Integer earliestReportIdx = null;
			Integer latestReportIdx = null;
			
			for (Report report : reports) {
				Integer lineItemIdx = findRnRLineItem(report, aggregatedLineItem);
				if(lineItemIdx != null){
					if(earliestReportIdx == null){
						earliestReportIdx = currentIdx;
					} else {
						if(compareReportChronologically(report, reports.get(earliestReportIdx)) < 0){
							earliestReportIdx = currentIdx;
						}
					}
					if(latestReportIdx == null){
						latestReportIdx = currentIdx;
					} else {
						if(compareReportChronologically(report, reports.get(latestReportIdx)) > 0){
							latestReportIdx = currentIdx;
						}
					}
					addRnRLineItem(aggregatedLineItem, report.getProducts().get(lineItemIdx), true);
					if(currentIdx.equals(reports.size() - 1)){
						aggregatedLineItem.setBeginningBalance(reports.get(earliestReportIdx).getProducts().get(lineItemIdx).getBeginningBalance());
						aggregatedLineItem.setStockInHand(reports.get(latestReportIdx).getProducts().get(lineItemIdx).getStockInHand());
					}
				}
				currentIdx++;
			}
			List<HubRnR> rnrs = fetchAllRnRsInProgram("ARV", facilityCode , HubRnR.FACILITY_MONTHLY);
			aggregatedLineItem.setAmc(computeAMC(rnrs, code));
			aggregatedReport.getProducts().add(aggregatedLineItem);
		}
		//TODO: aggregated report other properties, do it better than this 
		if(!reports.isEmpty()){
			aggregatedReport.setApproverName(reports.get(0).getAgentCode());
			aggregatedReport.setAgentCode(reports.get(0).getAgentCode());
			aggregatedReport.setEmergency(false);
			aggregatedReport.setProgramCode("ARV");
			aggregatedReport.setPeriodId(periodId);
		}
		return aggregatedReport;
	}
	
	public List<String> getProductCodes(List<Report> reports){
		List<String> productCodes = new ArrayList<String>();
		for (Report report : reports) {
			for (RnrLineItem item : report.getProducts()) {
				if(!productCodeExistsInList(item.getProductCode() ,productCodes)){
					productCodes.add(item.getProductCode());
				}
			}
		}
		return productCodes;
	}
	
	public boolean productCodeExistsInList(String productCode, List<String> codes){
		for (String code : codes) {
			if(code.equalsIgnoreCase(productCode)){
				return true;
			}
		}
		return false;
	}

	private Integer compareReportChronologically(Report report1, Report report2){
		ProcessingPeriod period1 = getProcessingPeriod(report1.getPeriodId());
		ProcessingPeriod period2 = getProcessingPeriod(report2.getPeriodId());
		if(period1.getStartdate().after(period2.getStartdate())){
			return 1;
		} else if (period1.getStartdate().before(period2.getStartdate())){
			return -1;
		} else {
			return 0;
		}
	}
	

	public Report aggregateMonthlyRnRsForFacilities(ArrayList<Report> reports , Integer periodId){
		Report aggregatedReport = new Report();
		aggregatedReport.setProducts(new ArrayList<RnrLineItem>());
		for (Report report : reports) {
			for (RnrLineItem rnrLineItem : report.getProducts()) {
				Integer lineItemIndex = findRnRLineItem(aggregatedReport, rnrLineItem);
				if(lineItemIndex == null){
					aggregatedReport.getProducts().add(rnrLineItem);
				} else {
					addRnRLineItem(aggregatedReport.getProducts().get(lineItemIndex), rnrLineItem , false);
				}
			}
		}
		
		List<HubRnR> rnrs = fetchAllRnRsInProgram("ARV", hubFacilityCode , HubRnR.AGGREGATED);
		for (RnrLineItem item : aggregatedReport.getProducts()) {
			item.setAmc(computeAMC(rnrs, item.getProductCode()));
		}
		aggregatedReport.setAgentCode(hubFacilityCode);
		aggregatedReport.setApproverName(hubApproverCode);
		aggregatedReport.setEmergency(false);
		aggregatedReport.setProgramCode("ARV");
		aggregatedReport.setPeriodId(periodId);
		return aggregatedReport;
	}
	
	private Integer findRnRLineItem(Report report,RnrLineItem rnrLineItem){
		for (int i=0 ; i < report.getProducts().size() ; i++) {
			if(report.getProducts().get(i).getProductCode().equals(rnrLineItem.getProductCode())){
				return i;
			}
		}
		return null;
	}
	
	private Integer findRnRLineItem(Report report,String code){
		for (int i=0 ; i < report.getProducts().size() ; i++) {
			if(report.getProducts().get(i).getProductCode().equals(code)){
				return i;
			}
		}
		return null;
	}
	
	private void addLossesAndAddjustments(RnrLineItem aggregateRnRLineItem , RnrLineItem newRnRLineItem){
		
			 for (LossesAndAdjustments lossesAndAdjustments : newRnRLineItem.getLossesAndAdjustments()) {
				LossesAndAdjustments lAndA = findLossesAndAdjustments(aggregateRnRLineItem, lossesAndAdjustments);
				if(lAndA == null){
					aggregateRnRLineItem.getLossesAndAdjustments().add(lossesAndAdjustments);
				} else {
					lAndA.setQuantity(((lAndA.getQuantity() != null)? lAndA.getQuantity() : 0) + ((lossesAndAdjustments.getQuantity() != null)? lossesAndAdjustments.getQuantity():0));
				}
			}
	}

	private LossesAndAdjustments findLossesAndAdjustments(RnrLineItem lineItem, LossesAndAdjustments lossesAndAdjustments ){
		for(int i=0; i< lineItem.getLossesAndAdjustments().size() ; i++){
			if(lineItem.getLossesAndAdjustments().get(i).getType().getId() == lossesAndAdjustments.getType().getId()){
				lineItem.getLossesAndAdjustments().get(i);
			}
		}
		return null;
	}
	
	private void addRnRLineItem(RnrLineItem aggregateRnRLineItem , RnrLineItem newRnRLineItem , boolean isWeekly){
		if(!isWeekly){
			aggregateRnRLineItem.setBeginningBalance(((aggregateRnRLineItem.getBeginningBalance() != null)? aggregateRnRLineItem.getBeginningBalance() : 0) + ((newRnRLineItem.getBeginningBalance() != null)?newRnRLineItem.getBeginningBalance():0));
			aggregateRnRLineItem.setStockInHand(((aggregateRnRLineItem.getStockInHand() != null)? aggregateRnRLineItem.getStockInHand() : 0) + ((newRnRLineItem.getStockInHand() != null)?newRnRLineItem.getStockInHand():0));
		}
		aggregateRnRLineItem.setQuantityDispensed(((aggregateRnRLineItem.getQuantityDispensed() != null)? aggregateRnRLineItem.getQuantityDispensed() : 0) + ((newRnRLineItem.getQuantityDispensed() != null)?newRnRLineItem.getQuantityDispensed():0));
		aggregateRnRLineItem.setQuantityReceived(((aggregateRnRLineItem.getQuantityReceived() != null)? aggregateRnRLineItem.getQuantityReceived() : 0) + ((newRnRLineItem.getQuantityReceived() != null)?newRnRLineItem.getQuantityReceived():0));
		aggregateRnRLineItem.setStockOutDays(((aggregateRnRLineItem.getStockOutDays() != null)? aggregateRnRLineItem.getStockOutDays() : 0) + ((newRnRLineItem.getStockOutDays() != null)?newRnRLineItem.getStockOutDays():0));
		aggregateRnRLineItem.setNewPatientCount(((aggregateRnRLineItem.getNewPatientCount() != null)? aggregateRnRLineItem.getNewPatientCount() : 0) + ((newRnRLineItem.getNewPatientCount() != null)?newRnRLineItem.getNewPatientCount():0));
		aggregateRnRLineItem.setQuantityRequested(((aggregateRnRLineItem.getQuantityRequested() != null)? aggregateRnRLineItem.getQuantityRequested() : 0) + ((newRnRLineItem.getQuantityRequested() != null)?newRnRLineItem.getQuantityRequested():0));
		aggregateRnRLineItem.setAmc(((aggregateRnRLineItem.getAmc() != null)? aggregateRnRLineItem.getAmc() : 0) + ((newRnRLineItem.getAmc() != null)?newRnRLineItem.getAmc():0));
		aggregateRnRLineItem.setNormalizedConsumption(((aggregateRnRLineItem.getNormalizedConsumption() != null)? aggregateRnRLineItem.getNormalizedConsumption() : 0) + ((newRnRLineItem.getNormalizedConsumption() != null)?newRnRLineItem.getNormalizedConsumption():0));
		aggregateRnRLineItem.setCalculatedOrderQuantity(((aggregateRnRLineItem.getCalculatedOrderQuantity() != null)? aggregateRnRLineItem.getCalculatedOrderQuantity() : 0) + ((newRnRLineItem.getCalculatedOrderQuantity() != null)?newRnRLineItem.getCalculatedOrderQuantity():0));
		aggregateRnRLineItem.setQuantityApproved(((aggregateRnRLineItem.getQuantityApproved() != null)? aggregateRnRLineItem.getQuantityApproved() : 0) + ((newRnRLineItem.getQuantityApproved() != null)?newRnRLineItem.getQuantityApproved():0));
		addLossesAndAddjustments(aggregateRnRLineItem, newRnRLineItem);
	}

	@Override
	public void saveRnR(HubRnR rnr) {
		// TODO Auto-generated method stub
		HubRnRDAO.getInstance().insertRnR(rnr);
	}

	@Override
	public String changeRnRStatus(HubRnR rnr, String status) {
		//TODO some dao method to update RnR status
		return null;
	}
	
	

	@Override
	public RnRSubmissionResult submitRnR(String rnr) {
		
		if(!Connectivity.isReachable(eLMISCentralURL)){
			return new RnRSubmissionResult(false,"FAILED","Unable to reach server. Please check your connection and try again.");
		}
		
		Boolean isSuccessful = false;
		String errorMsg = null;
		
		try {
			isSuccessful = (Boolean) BaseFactory.uploadJSON("sdp-requisitions", "R&R", rnr , Long.class);
		}catch(JSONException ex){
			if(ex.getMessage().contains("must begin with")){
				 errorMsg = "User doesn't have permission to submit R&R.";
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errorMsg = e.getMessage();
			if(e.getMessage().contains("full authentication required")){
				errorMsg = "User doesn't have permission to submit R&R.";
			}
		}
		if(isSuccessful){
//TODO: update submitted			
			return new RnRSubmissionResult(isSuccessful,"SUCCESSFUL","R&R successfully submitted!");
		} else {
//TODO: update failed submission
			return new RnRSubmissionResult(isSuccessful,"FAILED",errorMsg);
		}

	}

	@Override
	public List<HubRnR> fetchMonthlyRnRsForPerid(Date startDate, Date endDate) {
		List<HubRnR> rnrs=  HubRnRDAO.getInstance().selectMontlyRnRsForPeriod(startDate, endDate);
		
		for (HubRnR hubRnR : rnrs) {
			Report report = new Gson().fromJson(hubRnR.getRnrJSON(), Report.class);
			hubRnR.setReport(report);
		}
		return rnrs;
	}
	
	@Override
	public List<HubRnR> fetchMonthlyRnRsForPerid(Integer periodId) {
		List<HubRnR> rnrs=  HubRnRDAO.getInstance().selectMontlyRnRsForPeriod(periodId);
		
		for (HubRnR hubRnR : rnrs) {
			Report report = new Gson().fromJson(hubRnR.getRnrJSON(), Report.class);
			hubRnR.setReport(report);
		}
		return rnrs;
	}
	
	@Override
	public List<HubRnR> fetchAllRnRsInProgram(String programCode ,String facilityCode , String type) {
		
		List<HubRnR> rnrs=  HubRnRDAO.getInstance().selectAllRnRs(programCode , facilityCode , type);
		
		for (HubRnR hubRnR : rnrs) {
			Report report = new Gson().fromJson(hubRnR.getRnrJSON(), Report.class);
			hubRnR.setReport(report);
		}
		
		return rnrs;
	}
	
	@Override
	public Integer computeAWC( String productCode , String facilityCode){
		List<HubRnR> rnrs = fetchAllRnRsInProgram("ARV", facilityCode , HubRnR.WEEKLY);
		return computeAMC(rnrs, productCode);
	}
	
	public Integer  computeAMC(List<HubRnR> rnrs, String productCode){
		//rnrs parameter must be sorted chronologically 
		if(rnrs.isEmpty()) return 0;//TODO: makes sense?
		int i , j; int qtyDispSum = 0;
		for (i = 0, j = 0; j < rnrs.size() && i < 3;  j++){
			RnrLineItem item = findRnRLineItem(rnrs.get(i),productCode);
			if(item != null && item.getQuantityDispensed() > 0){
				qtyDispSum += item.getQuantityDispensed();
				i++;
			}
		}
		if (i == 0) return 0;
		return qtyDispSum/(i);
	}
	
	private RnrLineItem findRnRLineItem(HubRnR hubRnR , String productCode){
		for (RnrLineItem item : hubRnR.getReport().getProducts()) {
			if(item.getProductCode().equals(productCode))
				return item;
		}
		return null;
	}

	@Override
	public List<ProcessingPeriod> fetchPeriodsForSubmission() {
		
		return HubRnRDAO.getInstance().fetchPeriodsToSubmitRnRFor();
	}
	
	@Override
	public ProcessingPeriod getProcessingPeriod(Integer id) {
		return HubRnRDAO.getInstance().getProcessingPeriod(id);
	}
	
	@Override
	public ProcessingPeriod getProcessingPeriodByRange(Date startDate , Date endDate) {
		return HubRnRDAO.getInstance().getProcessingPeriodByRange(startDate ,endDate);
	}

	@Override
	public Integer updateUser(User user) {
		
		return HubRnRDAO.getInstance().updateUser(user);
	}

	@Override
	public Integer insertUser(User user) {
		
		return HubRnRDAO.getInstance().insertUser(user);
	}

	@Override
	public String aggregateHubRnR(ArrayList<HubRnR> rnrs , Integer periodId) {
		Gson gson = new Gson();
		String aggregatedRnR = null;
		ArrayList<String> rnrJSONs = new ArrayList<String>();
		for (HubRnR hubRnR : rnrs) {
			String rnrJson = gson.toJson(hubRnR.getReport());
			System.out.println(rnrJson);
			hubRnR.setRnrJSON(rnrJson);
			rnrJSONs.add(hubRnR.getRnrJSON());
		}
		aggregatedRnR = this.aggregateMonthlyRnR(rnrJSONs , periodId);
		Report report = gson.fromJson(aggregatedRnR, Report.class);
		System.out.println(report.getProducts().get(0).getQuantityApproved());
		return aggregatedRnR;
	}

	@Override
	public LoginRequestResult login(User user) {
		
		User returnedUser = HubRnRDAO.getInstance().findUser(user.getUserName());
		
		LoginRequestResult result =  new LoginRequestResult();
		
		if(returnedUser == null){
			result.setSuccess(false);
			result.setRemark("Invalid user name!");
		} else {
			if(user.getPassword().equals(returnedUser.getPassword()) && returnedUser.getActive()){
				result.setSuccess(true);
				result.setRemark("Login successful!");
			} else {
				result.setSuccess(false);
				result.setRemark("Invalid password!");
			}
		}
		
		return result;
	}
	
	private List<Report> extractReportsformHubRnRs(List<HubRnR> rnrs){
		List<Report> reports = new ArrayList<Report>();
		for (HubRnR hubRnR : rnrs) {
			reports.add(hubRnR.getReport());
		}
		return reports;
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return HubRnRDAO.getInstance().findAllUsers();
	}

	@Override
	public Integer insertRnR(HubRnR rnr) {
		return HubRnRDAO.getInstance().insertRnR(rnr);
	}

	@Override
	public Boolean insertWeeklyPeriods(List<ProcessingPeriod> weeklyPeriods) {
		for (ProcessingPeriod weeklyPeriod : weeklyPeriods) {
			try{
				HubRnRDAO.getInstance().insertWeeklyPeriod(weeklyPeriod);
			}catch(Exception ex){
				return false;
			}
		}
		return true;
	}

	@Override
	public Integer findScheduleIdByPeriodId(Integer periodId) {
		return HubRnRDAO.getInstance().findScheduleIdByPeriodId(periodId);
	}

	@Override
	public List<ProcessingPeriod> fetchWeeklyPeriods() {
		return HubRnRDAO.getInstance().fetchWeeklyPeriods();
	}
	
	@Override
	public List<ProcessingPeriod> fetchAllPeriods() {
		return HubRnRDAO.getInstance().fetchAllPeriods();
	}

	@Override
	public Integer updateRnRStatus(List<HubRnR> hubRnRList, String status) {
		return HubRnRDAO.getInstance().updateRnRStatus(hubRnRList, status);
	}
}
