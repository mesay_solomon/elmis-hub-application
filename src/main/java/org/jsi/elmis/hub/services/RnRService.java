package org.jsi.elmis.hub.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.hub.model.ProcessingPeriod;
import org.jsi.elmis.hub.model.User;
import org.jsi.elmis.hub.model.rnr.HubRnR;
import org.jsi.elmis.hub.request.service.LoginRequestResult;
import org.jsi.elmis.hub.request.service.RnRRequestResult;
import org.jsi.elmis.hub.rest.result.RnRSubmissionResult;

public interface RnRService {
	
	public String aggregateRnR(ArrayList<String> rnrs , ArrayList<String> facilityCodes , Integer periodId);
	
	public String aggregateHubRnR(ArrayList<HubRnR> rnrs , Integer periodId);
	
	// for approval, and possibly authorization - check business model
	public String changeRnRStatus(HubRnR rnr , String status);
	
	// submit aggregated RnR to MSL
	public RnRSubmissionResult submitRnR(String rnr);
	
	public List<ProcessingPeriod> fetchPeriodsForSubmission();
	
	public Integer findScheduleIdByPeriodId(Integer periodId);
	
	public Boolean insertWeeklyPeriods(List<ProcessingPeriod> weeklyPeriods);
	
	public List<User> findAllUsers();
	
	public Integer updateUser(User user);
	
	public Integer insertUser(User user);
	
	public LoginRequestResult login(User user);

	public Integer insertRnR(HubRnR rnr);
	
	public List<ProcessingPeriod> fetchWeeklyPeriods();
	
	public ProcessingPeriod getProcessingPeriod(Integer id);

	public void saveRnR(HubRnR rnr);

	public List<HubRnR> fetchAllRnRsInProgram(String programCode, String facilityCode,
			String type);

	public ProcessingPeriod getProcessingPeriodByRange(Date startDate, Date endDate);

	public Integer computeAWC(String productCode, String facilityCode);

	public List<ProcessingPeriod> fetchAllPeriods();

	public String aggregateMonthlyRnR(ArrayList<String> rnrs, Integer periodId);

	public List<HubRnR> fetchMonthlyRnRsForPerid(Date startDate, Date endDate);

	public List<HubRnR> fetchMonthlyRnRsForPerid(Integer periodId);

	public Integer updateRnRStatus(List<HubRnR> hubRnRList, String status);
}
