package org.jsi.elmis.hub;

import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.hub.dao.HubRnRDAO;
import org.jsi.elmis.hub.model.User;
import org.jsi.elmis.hub.model.rnr.HubRnR;
import org.jsi.elmis.hub.services.RnRServiceImpl;

import com.google.gson.Gson;

public class LocalTest {
	private static String rnr1 = "{\"agentCode\":\"101000\",\"programCode\":\"ARV\",\"periodId\":204,\"approverName\":\"wbomett\",\"emergency\":true,\"products\":[{\"productCode\":\"ARV0032\",\"beginningBalance\":0,\"quantityReceived\":43212,\"quantityDispensed\":2,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":6,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0025\",\"beginningBalance\":0,\"quantityReceived\":12342,\"quantityDispensed\":0,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"}]}";;
	private static String rnr2 = "{\"agentCode\":\"101000\",\"programCode\":\"ARV\",\"periodId\":205,\"approverName\":\"wbomett\",\"emergency\":true,\"products\":[{\"productCode\":\"ARV0032\",\"beginningBalance\":0,\"quantityReceived\":43212,\"quantityDispensed\":2,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":6,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0025\",\"beginningBalance\":0,\"quantityReceived\":12342,\"quantityDispensed\":0,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"}]}";;
	private static String rnr3 = "{\"agentCode\":\"101000\",\"programCode\":\"ARV\",\"periodId\":206,\"approverName\":\"wbomett\",\"emergency\":true,\"products\":[{\"productCode\":\"ARV0032\",\"beginningBalance\":0,\"quantityReceived\":43212,\"quantityDispensed\":2,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":6,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0025\",\"beginningBalance\":0,\"quantityReceived\":12342,\"quantityDispensed\":0,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"}]}";;
	private static String rnr4 = "{\"agentCode\":\"101000\",\"programCode\":\"ARV\",\"periodId\":207,\"approverName\":\"wbomett\",\"emergency\":true,\"products\":[{\"productCode\":\"ARV0032\",\"beginningBalance\":0,\"quantityReceived\":43212,\"quantityDispensed\":2,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":6,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0025\",\"beginningBalance\":0,\"quantityReceived\":12342,\"quantityDispensed\":0,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"}]}";;
	private static String rnr5 = "{\"agentCode\":\"101000\",\"programCode\":\"ARV\",\"periodId\":208,\"approverName\":\"wbomett\",\"emergency\":true,\"products\":[{\"productCode\":\"ARV0032\",\"beginningBalance\":0,\"quantityReceived\":43212,\"quantityDispensed\":2,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":6,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0025\",\"beginningBalance\":0,\"quantityReceived\":12342,\"quantityDispensed\":0,\"lossesAndAdjustments\":[],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"}]}";;
	public static void main(String args[]){
		testAggregateRnR();
//		testInsertRnR(new HubRnR());
//		testFetchHubRnRs();
//		testLogin();
//		testInsertUser();
//		testUpdateUser();
//		testUserToJson();
//		testFetchFacilities();
		
		
	}
	
	/**
	 * 
	 */
	private static void testFetchFacilities() {
		RnRServiceImpl aggregator = new RnRServiceImpl();aggregator.getFacilityCodes();
		List<String> fs = aggregator.getFacilityCodes();
		for (String f : fs) {
			System.out.println(f);
		}
	}

	private static void testUserToJson() {
		
		User user = new User();
		
		user.setUserName("mesay");
		user.setPassword("240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9");
		
		System.out.println(new Gson().toJson(user));
		
	}

	public static void testFetchHubRnRs(){
		List<HubRnR> rnrs = HubRnRDAO.getInstance().selectMontlyRnRsForPeriod(null, null);
	}
	public static void testAggregateRnR(){
		RnRServiceImpl aggregator = new RnRServiceImpl();aggregator.getFacilityCodes();
		ArrayList<String> rnrs = new ArrayList<String>();
		rnrs.add(rnr1);rnrs.add(rnr2);rnrs.add(rnr3);rnrs.add(rnr4);rnrs.add(rnr5);
		System.out.println(aggregator.aggregateRnR(rnrs , (ArrayList<String>)aggregator.getFacilityCodes() , 204) );
	}
	
	public static void testInsertRnR(HubRnR hubRnR){
		
		hubRnR.setFacilityCode("1011000");
		hubRnR.setProgramCode("ARV");
		hubRnR.setStatus("PENDING");
		String json = "{\"agentCode\":\"210027\",\"programCode\":\"ARV\",\"periodId\":6,\"approverName\":\"fkapyela\",\"emergency\":false,\"products\":[{\"productCode\":\"ARV0011\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":39,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":117,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0012\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":9,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0016\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":34,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":72,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0018\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":20,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0032\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":62,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0050\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":12,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":36,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0024\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":0,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0025\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":79,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":237,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0002\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":40,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0005\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":30,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"},{\"productCode\":\"ARV0047\",\"beginningBalance\":0,\"quantityReceived\":0,\"quantityDispensed\":1332,\"lossesAndAdjustments\":[{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":154},{\"type\":{\"name\":\"LOST\"},\"quantity\":-96},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":60},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":54},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":10},{\"type\":{\"name\":\"LOST\"},\"quantity\":-122},{\"type\":{\"name\":\"TRANSFER_IN\"},\"quantity\":1680}],\"stockInHand\":1,\"newPatientCount\":1,\"quantityRequested\":0,\"stockOutDays\":0,\"reasonForRequestedQuantity\":\"RnR\"}]}";
		hubRnR.setRnrJSON(json);
		hubRnR.setType("AGGREGATED");
		hubRnR.setParentRnR(new HubRnR());
		
		for(int i=0;i<5;i++)
		HubRnRDAO.getInstance().insertRnR(hubRnR);
	}
	
	public static void testLogin(){
		
		User user = new User();
		user.setUserName("mesay");
		user.setPassword("admin123");
		System.out.println(new RnRServiceImpl().login(user).getRemark());
		user.setPassword("Admin124");
		System.out.println(new RnRServiceImpl().login(user).getRemark());
		user.setUserName("jambo");
		System.out.println(new RnRServiceImpl().login(user).getRemark());
		
	}
	
	public static void testInsertUser(){
		User user = new User();
		
		user.setActive(true);
		user.setFirstName("Milkias");
		user.setLastName("Taye");
		user.setPassword("admin123");
		user.setUserName("milki");
		
		new RnRServiceImpl().insertUser(user);
	}
	
	public static void testUpdateUser(){
		User user = new User();
		
		user.setId(2);
		user.setActive(true);
		user.setFirstName("Mesay");
		user.setLastName("Taye");
		user.setPassword("240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9");
		user.setUserName("mesay");
		
		new RnRServiceImpl().updateUser(user);
	}
}
