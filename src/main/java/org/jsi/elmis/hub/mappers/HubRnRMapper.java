package org.jsi.elmis.hub.mappers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.hub.model.Facility;
import org.jsi.elmis.hub.model.ProcessingPeriod;
import org.jsi.elmis.hub.model.User;
import org.jsi.elmis.hub.model.rnr.HubRnR;


public interface HubRnRMapper {
	
	Integer insertRnR(HubRnR hubRnR);
	
	ArrayList<HubRnR> selectMontlyRnRsForPeriod(@Param("startDate")Date startDate , 
			@Param("endDate")Date endDate);
	
	ArrayList<HubRnR> selectAllRnRs(@Param("programCode")String programCode , 
			@Param("facilityCode") String facilityCode, @Param("type") String type);
	
	Integer updateRnR(HubRnR hubRnR);
	
	ArrayList<ProcessingPeriod> selectPeriodsToSubmitRnRFor();
	
	User selectUser(@Param("userName") String userName);
	
	ArrayList<User> selectAllUsers();
	
	Integer insertUser(User user);
	
	Integer updateUser(User user);
	
	Integer findScheduleIdByPeriodId(Integer periodId);
	
	Integer insertWeeklyProcessingPeriod(ProcessingPeriod weeklyProcessingPeriod);
	
	ArrayList<ProcessingPeriod> selectWeeklyPeriods();
	
	ProcessingPeriod selectPerocessingPeriod(@Param("id") Integer id);
	
	List<Facility> selectFacilities();

	ProcessingPeriod selectPerocessingPeriodByRange(@Param("startDate")Date startDate, @Param("endDate")Date endDate);

	ArrayList<ProcessingPeriod> selectAllPeriods();

	public List<HubRnR> selectMontlyRnRsForPeriodByPeriodId(@Param("periodId") Integer periodId);
	
	int updateRnRStatus(HubRnR rnr);
}


