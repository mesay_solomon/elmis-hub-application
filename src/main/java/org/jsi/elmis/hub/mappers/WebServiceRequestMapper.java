package org.jsi.elmis.hub.mappers;

import org.jsi.elmis.hub.request.service.WebServiceRequest;


public interface WebServiceRequestMapper {
	
	Integer insertRequest(WebServiceRequest wsRequest);
	
	WebServiceRequest selectLastPendingRequest();
	
	Integer updateRequestStatusByUUID(WebServiceRequest wsRequest);

}
