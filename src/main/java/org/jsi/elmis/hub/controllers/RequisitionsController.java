package org.jsi.elmis.hub.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.jsi.elmis.hub.model.ProcessingPeriod;
import org.jsi.elmis.hub.model.Test;
import org.jsi.elmis.hub.model.User;
import org.jsi.elmis.hub.model.rnr.HubRnR;
import org.jsi.elmis.hub.request.service.LoginRequestResult;
import org.jsi.elmis.hub.request.service.ResponseEntity;
import org.jsi.elmis.hub.rest.request.AggregateRequest;
import org.jsi.elmis.hub.rest.request.UpdateRnRRequest;
import org.jsi.elmis.hub.rest.request.WeeklyPeriodsRequest;
import org.jsi.elmis.hub.rest.result.RnRSubmissionResult;
import org.jsi.elmis.hub.services.JSONUtil;
import org.jsi.elmis.hub.services.RnRService;
import org.jsi.elmis.hub.services.RnRServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

@Controller
public class RequisitionsController {
	
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private final static String SUCCESS = "SUCCESS";
    private final static String FAILURE = "FAILURE";
    private final static String PENDING = "PENDING";
    
    RnRService rnrService = new RnRServiceImpl();
	
    @RequestMapping("/greeting")
    public @ResponseBody Test greeting(
            @RequestParam(value="name", required=false, defaultValue="World") String name) {
        return new Test(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
    @RequestMapping(value="/rest-api/sdp-requisitions", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity saveRnR(@RequestBody String rnrJson ){
//    	Gson gson = new Gson();
    	//Report report = gson.fromJson(rnrJson, Report.class);
    	HubRnR hubRnR = new HubRnR(rnrJson , HubRnR.PENDING);
    	rnrService.saveRnR(hubRnR);
    	return new ResponseEntity(0, "R&R submitted successfully!");
    }
 
    @RequestMapping(value = "/rest-api/rnr/aggregated/submission", method = RequestMethod.POST)
    public @ResponseBody RnRSubmissionResult receiveApprovedRnRs(@RequestBody AggregateRequest request) {
    	System.out.println(request.getRnrsJSON());
    	ArrayList<HubRnR> rnrs = JSONUtil.parseHubRnRs(request.getRnrsJSON());
    	String aggregatedRnR = rnrService.aggregateHubRnR(rnrs , request.getPeriodId());
    	
//    	RnRRequestResult requestResult = rnrService.submitRnR(aggregatedRnR);
//    	save aggregated rnr and update child rnrs
    	return  rnrService.submitRnR(aggregatedRnR);
    }
    
    @RequestMapping(value = "/rest-api/rnr/monthly/{periodid}", method = RequestMethod.GET)
    public @ResponseBody ArrayList<HubRnR> rnrsForPeriod(
            @PathVariable(value="periodid") Integer periodId) {
        return (ArrayList<HubRnR>) rnrService.fetchMonthlyRnRsForPerid(periodId);
    }
    
    @RequestMapping(value = "/rest-api/lookup/processing-periods", method = RequestMethod.GET)
    public @ResponseBody ArrayList<ProcessingPeriod> allPeriods() {
        return (ArrayList<ProcessingPeriod>) rnrService.fetchAllPeriods();
    }
    
    @RequestMapping(value = "/rest-api/lookup/weekly/processing-periods", method = RequestMethod.GET)
    public @ResponseBody ArrayList<ProcessingPeriod> weeklyPeriods() {
        return (ArrayList<ProcessingPeriod>) rnrService.fetchWeeklyPeriods();
    }
    
    @RequestMapping(value = "/rest-api/monthly-periods", method = RequestMethod.GET)
    public @ResponseBody ArrayList<ProcessingPeriod> periodsForSubmission() {
        return (ArrayList<ProcessingPeriod>) rnrService.fetchPeriodsForSubmission();
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public @ResponseBody LoginRequestResult login(@RequestBody String userString) {
    	System.out.println(userString);
    	User user = new Gson().fromJson(userString, User.class);
    	return  rnrService.login(user);
    }
    
    @RequestMapping(value = "/create-user", method = RequestMethod.POST)
    public @ResponseBody boolean createUser(@RequestBody String userString) {
    	System.out.println(userString);
    	User user = new Gson().fromJson(userString, User.class);
    	return rnrService.insertUser(user) > 0;
    }
    
    @RequestMapping(value = "/update-user", method = RequestMethod.POST)
    public @ResponseBody boolean updateUser(@RequestBody String userString) {
    	System.out.println(userString);
    	User user = new Gson().fromJson(userString, User.class);
    	return  rnrService.updateUser(user) > 0;
    }
    
    @RequestMapping("/find-all-users")
    public @ResponseBody List<User> findUsers() {
        return rnrService.findAllUsers();
    }
    
    @RequestMapping(value = "/create-weekly-periods", method = RequestMethod.POST)
    public @ResponseBody Boolean createWeeklyPeriods(@RequestBody String jsonWeeklyPeriodRequest) {
    	
    	Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
			@Override
			public Date deserialize(JsonElement json, Type typeOfT,
					JsonDeserializationContext context)
					throws JsonParseException {
				return new Date(json.getAsJsonPrimitive().getAsLong());
			}
		}).create();
    	
    	
    	WeeklyPeriodsRequest wPeriodRequest = gson.fromJson(jsonWeeklyPeriodRequest, WeeklyPeriodsRequest.class);
    	Integer scheduleId = rnrService.findScheduleIdByPeriodId(wPeriodRequest.getParentPeriodId());
    	
    	for(ProcessingPeriod pp : wPeriodRequest.getWeeklyPeriods()){
    		pp.setParentperiodid(wPeriodRequest.getParentPeriodId());
    		pp.setScheduleid(scheduleId);
    	}
    	
        return rnrService.insertWeeklyPeriods(wPeriodRequest.getWeeklyPeriods());
    }
    
    @RequestMapping(value = "/weekly-periods", method = RequestMethod.GET)
    public @ResponseBody ArrayList<ProcessingPeriod> getWeeklyPeriods() {
        return (ArrayList<ProcessingPeriod>) rnrService.fetchWeeklyPeriods();
    }
    
    @RequestMapping("/awc")
    public @ResponseBody Integer computeAWC(
            @RequestParam(value="productCode", required=false, defaultValue="World") String productCode,
            @RequestParam(value="facilityCode", required=false, defaultValue="World") String facilityCode) {
        return rnrService.computeAWC(productCode, facilityCode);
    }
    
    @RequestMapping(value = "/rest-api/rnr/update-status", method = RequestMethod.POST)
    public @ResponseBody Integer updateRnRStatus(@RequestBody UpdateRnRRequest updateRnRRequest) {
        return rnrService.updateRnRStatus(updateRnRRequest.getRnrList(), updateRnRRequest.getStatus());
    }
}
